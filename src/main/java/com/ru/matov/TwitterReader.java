package com.ru.matov;

import twitter4j.StreamListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;

/**
 * Чтение сообщений из Твиттера
 */
public class TwitterReader {


    public static void getMessagesFromTwitter() {

        // Создание конфигурации
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setOAuthAccessToken("1272243708-d79qx8KCGqvt54woj6N6yKd1csctlz3ukyg4sQx")
          .setOAuthAccessTokenSecret("w9jc2LLrrFZFzMspcN7Ms3s7a7tvNKff3oLb6KCNhUbnI")
          .setOAuthConsumerKey("xjWRFm4TIBZkvIwo1QzAoQ")
          .setOAuthConsumerSecret("4Xv2Vitc9FYGliwxdDv0g09YdIuK17JrCaQ3WZg");

        //Property для записи в kafka
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT: //localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.LongSerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");

        //Инициализация Producer-a
        RecordProducer recordProducer = new RecordProducer(configProperties);
        StreamListener streamListener = new TwitterRawStreamListener(recordProducer);

        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        twitterStream.addListener(streamListener);

        twitterStream.sample();
    }
}
