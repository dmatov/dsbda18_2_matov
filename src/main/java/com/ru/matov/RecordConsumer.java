package com.ru.matov;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.util.Progressable;

public class RecordConsumer {

    private final String topic;
    private final Properties configProperties;

    public RecordConsumer() {

        /**
         * Название топика
         */
        this.topic = "test111";

        /**
         * Инициализация проперрти для Consumer-a
         */
        configProperties = new Properties();
        configProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT: //localhost:9092");
        configProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.LongDeserializer");
        configProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
        configProperties.put("group.id", "-consumer-" + System.currentTimeMillis());
        configProperties.put("session.timeout.ms",100000);
        configProperties.put("max.poll.records",10000);

    }

    /**
     * Метод слушателя Kafka
     */
    public void consume() throws InterruptedException, IOException {

    try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(configProperties)) {

        //Подписка на топик
        consumer.subscribe(Arrays.asList(topic));

        //Создание Spark-сессии
        SparkSession session = SparkSession
                .builder()
                .master("local")
                .appName("Hw2")
                .getOrCreate();

        //Конфиг для записи в файл
        Configuration conf = new Configuration();

        //Создание Spark-контекста
        JavaSparkContext javaSparkContext = new JavaSparkContext(session.sparkContext());

        //Бесконечный фикл опроса kafka. Выход из цикла при остановке программы
        boolean a = true;
        while (a) {
            System.out.println("Start consume");
            //Sleep для считывания поминутно
            Thread.sleep(60000);
            ConsumerRecords<String, String> records = consumer.poll(1000);
            ArrayList<String> metrics = new ArrayList<>();
            for (ConsumerRecord<String, String> record : records) {
                metrics.add(record.value());
            }

            JavaRDD<String> metricRdd = javaSparkContext.parallelize(metrics);

            //Количество записей
            long count = metricRdd.count();

            //Получение текущего вемени для названия файла
            DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd-HH.mm.ss");
            Date date = new Date();
            String dateTimestampWithMinutesAccuracy = dateFormat.format(date);


            System.out.println("Count of delete message per minute is " + count);
            String dest = "Count of delete message per minute is " + count;

            //Создание файла для текущей итерации считывания
            String Dest = "hdfs://localhost:9000/hw2"+"-"+dateTimestampWithMinutesAccuracy;
            FileSystem fs = FileSystem.get(URI.create(Dest),conf);
            Path path = new Path(Dest);

            //Запись в HDFS средствами IOUtils
           if(!fs.exists(path)) {
               OutputStream out = fs.create(path, new Progressable() {
                   public void progress() {
                       System.out.print(".");
                   }
               });
               InputStream stream = new ByteArrayInputStream(dest.getBytes(StandardCharsets.UTF_8));
               System.out.println();
               IOUtils.copyBytes(stream, out, 4096, true);
               IOUtils.closeStream(stream);
               IOUtils.closeStream(out);
           }

        }
    }

    }
}
