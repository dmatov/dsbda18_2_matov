package com.ru.matov;

import lombok.extern.slf4j.Slf4j;
import twitter4j.RawStreamListener;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.time.DateUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Обработчик потока сообщений
 */
@Slf4j
public class TwitterRawStreamListener implements RawStreamListener {

    private RecordProducer recordProducer;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public TwitterRawStreamListener(RecordProducer recordProducer) {
        this.recordProducer = recordProducer;
    }

    /**
     * Обработка сообщения из Твиттера. При поступлении сообщения отправляет в Producer-a
     * */
    public void onMessage(String s) {
        try {
            HashMap<String, Object> event = objectMapper.readValue(s, HashMap.class);
            Object deletionLabel = event.get("delete");
            //Проверка типа события
            if(deletionLabel != null) {
                System.out.println("Caught deletion message from Twitter: " + s);
                Long dateTimestampWithMinutesAccuracy = DateUtils.truncate(new Date(), Calendar.MINUTE).getTime();
                recordProducer.pushToTopic(dateTimestampWithMinutesAccuracy, s);
            }
        } catch (Exception e ) {}
    }

    public void onException(Exception e) {}
}
