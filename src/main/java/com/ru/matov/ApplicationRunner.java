package com.ru.matov;

import java.util.*;


public class ApplicationRunner {

    private static Properties props = new Properties();

    /**
     * Название топика
     */
    public static final String TOPIC = "test111";

    /**
     * Инициализация настроек для Producer-a
     */
    static {
        props.put("bootstrap.servers", "PLAINTEXT://localhost:9092");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.LongDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("group.id", "-consumer-" + System.currentTimeMillis());
        props.put("session.timeout.ms",80000);
        props.put("auto.offset.reset", "earliest");
    }


    public static void main(String[] args) throws Exception {

        TwitterReader.getMessagesFromTwitter();
        RecordConsumer session = new RecordConsumer();
        session.consume();

    }


}

