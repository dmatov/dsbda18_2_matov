package com.ru.matov;

import lombok.extern.slf4j.Slf4j;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 * Запись в Kafka
 */
@Slf4j
public class RecordProducer {


    private static Producer<Long, String> producer;


    public RecordProducer(Properties configProperties) {
        producer = new KafkaProducer<>(configProperties);
    }

    /**
     * Запись в топик
     */
    public void pushToTopic(Long dateTimestamp, String event) {

        ProducerRecord<Long, String> rec = new ProducerRecord<>(ApplicationRunner.TOPIC, dateTimestamp, event);
        producer.send(rec);

    }



}