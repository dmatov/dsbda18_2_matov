package com.ru.matov;

import org.apache.curator.test.TestingServer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import kafka.server.KafkaConfig;
import kafka.server.KafkaServerStartable;
import java.util.Arrays;
import java.util.Properties;

public class KafkaServerTest {

    public Properties buildTestConsumerProperties() {
        Properties consumerProperties = new Properties();
        consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        consumerProperties.put("group.id", "test");
        consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.LongDeserializer");
        consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        return consumerProperties;
    }

    public Properties buildTestProducerProperties() {
        Properties producerProperties = new Properties();
        producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.LongSerializer");
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");
        return producerProperties;
    }

    @Test
    public void testRecordProducer() throws InterruptedException {
        Properties consumerProperties = buildTestConsumerProperties();
        KafkaConsumer<Long, String> recordConsumer = new KafkaConsumer<>(consumerProperties);
        recordConsumer.subscribe(Arrays.asList(ApplicationRunner.TOPIC));
        // Смотрим, сколько изначально было записей в очереди
        recordConsumer.poll(1000);


        Properties producerProperties = buildTestProducerProperties();
        RecordProducer recordProducer = new RecordProducer(producerProperties);
        recordProducer.pushToTopic(1L, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        recordProducer.pushToTopic(2L, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        ConsumerRecords<Long, String> consumerRecordsAfter = recordConsumer.poll(1000);
        Assert.assertEquals(2, consumerRecordsAfter.count());
        recordConsumer.close();
    }


}