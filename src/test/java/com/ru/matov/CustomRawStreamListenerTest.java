package com.ru.matov;

import lombok.extern.slf4j.Slf4j;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

@Slf4j
public class CustomRawStreamListenerTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    private KafkaServerTest mockKafkaServerTest = new KafkaServerTest();


    @Test
    public void onDeleteMessage() throws Exception {

        Properties consumerProperties = mockKafkaServerTest.buildTestConsumerProperties();
        KafkaConsumer<Long, String> recordConsumer = new KafkaConsumer<>(consumerProperties);
        recordConsumer.subscribe(Arrays.asList(ApplicationRunner.TOPIC));

        Properties producerProperties = mockKafkaServerTest.buildTestProducerProperties();
        RecordProducer recordProducer = new RecordProducer(producerProperties);
        TwitterRawStreamListener customRawStreamListener = new TwitterRawStreamListener(recordProducer);
        Map<String, String> messageAsMap = new HashMap<>();
        messageAsMap.put("delete", "xxx");
        messageAsMap.put("xxx", "xxx");

        String deleteMessage = objectMapper.writeValueAsString(messageAsMap);
        customRawStreamListener.onMessage(deleteMessage);

        ConsumerRecords<Long, String>  afterConsumerRecords = recordConsumer.poll(1000);
        int consumedCount =  afterConsumerRecords.count();

        recordConsumer.close();
        Assert.assertEquals(1, consumedCount);

    }

    @Test
    public void onNotDeleteMessage() throws Exception {

        Properties consumerProperties = mockKafkaServerTest.buildTestConsumerProperties();
        KafkaConsumer<Long, String> recordConsumer = new KafkaConsumer<>(consumerProperties);
        recordConsumer.subscribe(Arrays.asList(ApplicationRunner.TOPIC));

        Properties producerProperties = mockKafkaServerTest.buildTestProducerProperties();
        RecordProducer recordProducer = new RecordProducer(producerProperties);
        TwitterRawStreamListener customRawStreamListener = new TwitterRawStreamListener(recordProducer);

        Map<String, String> messageAsMap = new HashMap<>();
        messageAsMap.put("xxxx", "xxxx");
        messageAsMap.put("xxx", "xxx");

        String deleteMessage = objectMapper.writeValueAsString(messageAsMap);
        customRawStreamListener.onMessage(deleteMessage);

        ConsumerRecords<Long, String>  afterConsumerRecords = recordConsumer.poll(1000);
        int consumedCount =  afterConsumerRecords.count();
        recordConsumer.close();

        Assert.assertEquals(0, consumedCount);

    }
}
